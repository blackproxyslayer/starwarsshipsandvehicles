package com.blackproxy.starwarsshipsandvehicles.api

import com.blackproxy.starwarsshipsandvehicles.model.StarshipsList
import com.blackproxy.starwarsshipsandvehicles.model.VehiclesList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

/**
 * Created by Boonya Kitpitak on 6/16/17.
 */
interface Swapi {

    @GET("starships/")
    fun getStarships(): Call<StarshipsList>

    @GET("vehicles/")
    fun getVehicles(): Call<VehiclesList>

    companion object Factory {
        @Volatile
        private var retrofit : Retrofit? = null

        private const val BASE_URL: String = "http://swapi.co/api/"

        @Synchronized
        fun getInstance(): Swapi? {
            retrofit ?: synchronized(this) {
                retrofit = buildRetrofit() //retrofit ?: buildRetrofit()
            }

            return retrofit?.create(Swapi::class.java)
        }

        private fun buildRetrofit() = retrofit2.Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}
