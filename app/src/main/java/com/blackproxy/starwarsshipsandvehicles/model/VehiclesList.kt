package com.blackproxy.starwarsshipsandvehicles.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
data class VehiclesList(@SerializedName("results") val vehiclesList: List<Vehicle>, val next: String?)