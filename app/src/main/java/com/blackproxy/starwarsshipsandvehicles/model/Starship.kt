package com.blackproxy.starwarsshipsandvehicles.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */

data class Starship(val name: String,
                    val model: String,
                    @SerializedName("starship_class") val spclass : String,
                    val manufacturer: String,
                    val length : String,
                    val crew: String,
                    val passengers: String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(model)
        parcel.writeString(spclass)
        parcel.writeString(manufacturer)
        parcel.writeString(length)
        parcel.writeString(crew)
        parcel.writeString(passengers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Starship> {
        override fun createFromParcel(parcel: Parcel): Starship {
            return Starship(parcel)
        }

        override fun newArray(size: Int): Array<Starship?> {
            return arrayOfNulls(size)
        }
    }

}