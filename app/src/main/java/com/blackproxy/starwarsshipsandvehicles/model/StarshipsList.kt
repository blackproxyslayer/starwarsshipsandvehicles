package com.blackproxy.starwarsshipsandvehicles.model

import com.google.gson.annotations.SerializedName

/**
 * Created by oozou on 7/12/2017 AD.
 */
data class StarshipsList(@SerializedName("results") val starshipsList: List<Starship>, val next: String?)