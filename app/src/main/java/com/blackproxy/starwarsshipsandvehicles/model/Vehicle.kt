package com.blackproxy.starwarsshipsandvehicles.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
/*

 */
data class Vehicle (val name: String,
                    val model: String,
                    @SerializedName( "vehicle_class")val vclass : String,
                    val manufacturer: String,
                    val lenght: String,
                    val crew: String,
                    val passengers: String): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(model)
        parcel.writeString(vclass)
        parcel.writeString(manufacturer)
        parcel.writeString(lenght)
        parcel.writeString(crew)
        parcel.writeString(passengers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Vehicle> {
        override fun createFromParcel(parcel: Parcel): Vehicle {
            return Vehicle(parcel)
        }

        override fun newArray(size: Int): Array<Vehicle?> {
            return arrayOfNulls(size)
        }
    }
}