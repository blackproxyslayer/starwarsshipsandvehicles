package com.blackproxy.starwarsshipsandvehicles.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blackproxy.starwarsshipsandvehicles.R
import com.blackproxy.starwarsshipsandvehicles.model.Starship
import com.blackproxy.starwarsshipsandvehicles.model.Vehicle
import kotlinx.android.synthetic.main.item_starship.view.*

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
class StarshipsAdapter(var items: List<Starship>) :
    RecyclerView.Adapter<StarshipsAdapter.StarshipsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StarshipsViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_starship, parent, false)

        return StarshipsViewHolder(v)
    }

    override fun onBindViewHolder(holder: StarshipsViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = items.size

    class StarshipsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val vehicleName = view.nameValue
        val vehicleModel = view.modelValue
        val vehicleClass = view.starshipClassValue
        val vehiclePassengers = view.passengersValue

        fun bind(starship: Starship){
            vehicleName.text = starship.name
            vehicleModel.text = starship.model
            vehicleClass.text = starship.spclass
            vehiclePassengers.text = starship.passengers
        }
    }
}