package com.blackproxy.starwarsshipsandvehicles.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blackproxy.starwarsshipsandvehicles.R
import com.blackproxy.starwarsshipsandvehicles.model.Vehicle
import kotlinx.android.synthetic.main.item_vehicle.view.*

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
class VehiclesAdapter(var items: List<Vehicle>) :
    RecyclerView.Adapter<VehiclesAdapter.VehiclesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehiclesViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_vehicle, parent, false)

        return VehiclesViewHolder(v)
    }

    override fun onBindViewHolder(holder: VehiclesViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = items.size

    class VehiclesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val vehicleName = view.nameValue
        val vehicleModel = view.modelValue
        val vehicleClass = view.vehicleClassValue
        val vehiclePassengers = view.passengersValue

        fun bind(vehicle: Vehicle){
            vehicleName.text = vehicle.name
            vehicleModel.text = vehicle.model
            vehicleClass.text = vehicle.vclass
            vehiclePassengers.text = vehicle.passengers
        }
    }
}