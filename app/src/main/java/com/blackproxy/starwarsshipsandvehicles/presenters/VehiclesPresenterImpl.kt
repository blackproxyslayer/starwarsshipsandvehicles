package com.blackproxy.starwarsshipsandvehicles.presenters

import com.blackproxy.starwarsshipsandvehicles.interactors.VehiclesInteractor
import com.blackproxy.starwarsshipsandvehicles.interactors.VehiclesInteractorImpl
import com.blackproxy.starwarsshipsandvehicles.model.Vehicle
import com.blackproxy.starwarsshipsandvehicles.views.VehiclesView

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
class VehiclesPresenterImpl(var view: VehiclesView?) : VehiclesPresenter {

    val vehiclesInteractor  : VehiclesInteractor = VehiclesInteractorImpl()

    override fun loadVehicles() {
        view?.showProgress()
        vehiclesInteractor.getVehicles(::onItemsLoaded, ::onItemsFailure)
    }

    private fun onItemsLoaded(items: List<Vehicle>?) {
        view?.apply {
            hideProgress()
            setItems(items)
        }
    }

    private fun onItemsFailure(error:Throwable?) {
        view?.apply {
            hideProgress()
            error?.message?.apply {
                showMessage(this)
            }

        }
    }

    override fun onItemClicked(item: Vehicle) {

        view?.showMessage("Vechicle clicked: ${item.name}")
    }

    override fun onDestroy() {
        view = null
    }

}