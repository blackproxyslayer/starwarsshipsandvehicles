package com.blackproxy.starwarsshipsandvehicles.presenters

import com.blackproxy.starwarsshipsandvehicles.interactors.StarshipsInteractor
import com.blackproxy.starwarsshipsandvehicles.interactors.StarshipsInteractorImpl
import com.blackproxy.starwarsshipsandvehicles.model.Starship
import com.blackproxy.starwarsshipsandvehicles.views.StarshipsView


/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
class StarshipsPresenterImpl(var view : StarshipsView?) : StarshipsPresenter {

    val starshipInteractor  : StarshipsInteractor = StarshipsInteractorImpl()

    var starshipList : List<Starship>? = emptyList()

    override fun loadStarships() {
        if (starshipList.isNullOrEmpty()){
            view?.showProgress()
            starshipInteractor.getStarships(::onItemsLoaded, ::onItemsFailure)
        }
    }

    private fun onItemsLoaded(items: List<Starship>?) {
        view?.apply {
            starshipList = items
            hideProgress()
            setItems(items)
        }
    }

    private fun onItemsFailure(error:Throwable?) {
        view?.apply {
            hideProgress()
            error?.message?.apply {
                showMessage(this)
            }

        }
    }

    override fun onItemClicked(item: Starship) {

        view?.showMessage("Vechicle clicked: ${item.name}")
    }

    override fun onDestroy() {
        view = null
    }
}