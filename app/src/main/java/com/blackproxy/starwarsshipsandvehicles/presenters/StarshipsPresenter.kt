package com.blackproxy.starwarsshipsandvehicles.presenters

import com.blackproxy.starwarsshipsandvehicles.model.Starship

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
interface StarshipsPresenter {

    fun loadStarships()

    fun onItemClicked(item: Starship)

    fun onDestroy()
}