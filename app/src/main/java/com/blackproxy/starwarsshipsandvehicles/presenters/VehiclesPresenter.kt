package com.blackproxy.starwarsshipsandvehicles.presenters

import com.blackproxy.starwarsshipsandvehicles.model.Vehicle

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
interface VehiclesPresenter {

    fun loadVehicles()

    fun onItemClicked(item: Vehicle)

    fun onDestroy()
}