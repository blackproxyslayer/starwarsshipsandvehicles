package com.blackproxy.starwarsshipsandvehicles.utils

data class FragmentState(val currentFragmentTag: String, var oldFragmentTag: String)
