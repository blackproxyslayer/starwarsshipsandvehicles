package com.blackproxy.starwarsshipsandvehicles.views

import com.blackproxy.starwarsshipsandvehicles.model.Vehicle

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
interface VehiclesView {
    fun showProgress()
    fun hideProgress()
    fun setItems(items: List<Vehicle>?)
    fun showMessage(message: String)
}