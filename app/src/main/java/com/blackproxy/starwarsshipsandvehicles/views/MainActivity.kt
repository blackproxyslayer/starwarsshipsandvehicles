package com.blackproxy.starwarsshipsandvehicles.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import com.blackproxy.starwarsshipsandvehicles.R
import com.blackproxy.starwarsshipsandvehicles.utils.FragmentState
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView {

    private val TAG_ONE = "first"
    private val TAG_SECOND = "second"
    private val MAX_HISTORIC = 2

    private var currentFragment: Fragment? = null

    private val listState = mutableListOf<FragmentState>()
    private var currentTag: String = TAG_ONE
    private var oldTag: String = TAG_ONE
    private var currentMenuItemId: Int = R.id.navigation_ships

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) loadFirstFragment()
        initUI()
    }

    override fun initUI() {

        navigation.setOnNavigationItemSelectedListener { menuItem ->

            if (currentMenuItemId != menuItem.itemId) {

                val fragment: Fragment
                oldTag = currentTag

                currentMenuItemId = menuItem.itemId

                when (currentMenuItemId) {
                    R.id.navigation_ships -> {
                        currentTag = TAG_ONE
                        fragment = StarshipsFragment.newInstance()
                        loadFragment(fragment, currentTag)
                    }
                    R.id.navigation_vehicles -> {
                        currentTag = TAG_SECOND
                        fragment = VehiclesFragment.newInstance()
                        loadFragment(fragment, currentTag)
                    }
                }
                return@setOnNavigationItemSelectedListener true
            }
            false
        }
    }

    override fun onBackPressed() {
        if (listState.size >= 1) {
            recoverFragment()
        } else {
            super.onBackPressed()
        }
    }

    private fun recoverFragment() {

        val lastState = listState.last()
        listState.removeAt(listState.size - 1)

        currentTag = lastState.currentFragmentTag
        oldTag = lastState.oldFragmentTag

        Log.d("thr recover", "$currentTag - $oldTag")

        val ft = supportFragmentManager.beginTransaction()

        val currentFragment = supportFragmentManager.findFragmentByTag(currentTag)
        val oldFragment = supportFragmentManager.findFragmentByTag(oldTag)


        if (currentFragment!!.isVisible && oldFragment!!.isHidden) {
            ft.hide(currentFragment).show(oldFragment)
        }

        ft.commit()

        val menu = navigation.menu

        when (oldTag) {
            TAG_ONE -> menu.getItem(0).isChecked = true
            TAG_SECOND -> menu.getItem(1).isChecked = true
        }

    }

    private fun loadFirstFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        currentFragment = StarshipsFragment.newInstance()
        transaction.add(R.id.flContainer, currentFragment!!, TAG_ONE)
        transaction.commit()
    }

    private fun loadFragment(fragment: Fragment, tag: String) {

        if (currentFragment !== fragment) {
            val ft = supportFragmentManager.beginTransaction()

            if (fragment.isAdded) {
                ft.hide(currentFragment!!).show(fragment)
            } else {
                ft.hide(currentFragment!!).add(R.id.flContainer, fragment, tag)
            }
            currentFragment = fragment

            ft.commit()

            addBackStack()
        }

    }

    //Like YouTube
    private fun addBackStack() {
        Log.d("thr add", "$currentTag - $oldTag")

        when (listState.size) {
            MAX_HISTORIC -> {

                listState[1].oldFragmentTag = TAG_ONE
                val firstState = listState[1]

                for (i in listState.indices) {
                    if (listState.indices.contains((i + 1))) {
                        listState[i] = listState[i + 1]
                    }
                }

                listState[0] = firstState
                listState[listState.lastIndex] = FragmentState(currentTag, oldTag)
            }
            else -> {
                listState.add(FragmentState(currentTag, oldTag))
            }
        }

    }

}
