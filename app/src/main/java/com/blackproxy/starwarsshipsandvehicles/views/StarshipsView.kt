package com.blackproxy.starwarsshipsandvehicles.views

import com.blackproxy.starwarsshipsandvehicles.model.Starship

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
interface StarshipsView {
    fun showProgress()
    fun hideProgress()
    fun setItems(items: List<Starship>?)
    fun showMessage(message: String)
}