package com.blackproxy.starwarsshipsandvehicles.views

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.blackproxy.starwarsshipsandvehicles.R
import com.blackproxy.starwarsshipsandvehicles.adapters.StarshipsAdapter
import com.blackproxy.starwarsshipsandvehicles.model.Starship
import com.blackproxy.starwarsshipsandvehicles.presenters.StarshipsPresenter
import com.blackproxy.starwarsshipsandvehicles.presenters.StarshipsPresenterImpl
import com.blackproxy.starwarsshipsandvehicles.utils.MarginItemDecoration
import kotlinx.android.synthetic.main.fragment_starships.*

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
class StarshipsFragment : Fragment(), StarshipsView {


    private val presenter : StarshipsPresenter = StarshipsPresenterImpl(this)

    private var starshipsAdapter = StarshipsAdapter(emptyList())

    private var starShipsList : ArrayList<Starship>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_starships, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
        if (savedInstanceState !=null){
            starShipsList = savedInstanceState.getParcelableArrayList(STARSHIP_LIST_KEY)
            setItems(starShipsList)
        } else {
            if (starShipsList != null) {
                //donothing
            } else {
                presenter.loadStarships()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(STARSHIP_LIST_KEY, starShipsList)
    }

    private fun initUI(){
        rvStarships.adapter = starshipsAdapter
        rvStarships.layoutManager = LinearLayoutManager(activity)
        rvStarships.addItemDecoration(MarginItemDecoration(resources.getDimension(R.dimen.default_padding).toInt()))
        swipeRefreshStarships.setOnRefreshListener {
            presenter.loadStarships()
        }
    }

    override fun showProgress() {
        swipeRefreshStarships.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefreshStarships.isRefreshing = false
    }

    override fun setItems(items: List<Starship>?) {
        if (!items.isNullOrEmpty()){
            starShipsList = ArrayList(items)
            starshipsAdapter.items = items
            refreshStarshipsList()
        }
    }

    private fun refreshStarshipsList(){
        starshipsAdapter.notifyDataSetChanged()
    }

    override fun showMessage(message: String) {
        Toast.makeText(activity,message, Toast.LENGTH_LONG).show()
    }

    companion object {

        const val STARSHIP_LIST_KEY =  "STARSHIP_LIST_KEY"

        @JvmStatic
        fun newInstance() : StarshipsFragment {
            val fragment = StarshipsFragment()
            val arguments = Bundle()
            fragment.arguments = arguments
            return fragment
        }
    }
}