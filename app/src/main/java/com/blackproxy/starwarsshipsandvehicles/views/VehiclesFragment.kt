package com.blackproxy.starwarsshipsandvehicles.views

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.blackproxy.starwarsshipsandvehicles.R
import com.blackproxy.starwarsshipsandvehicles.adapters.VehiclesAdapter
import com.blackproxy.starwarsshipsandvehicles.model.Vehicle
import com.blackproxy.starwarsshipsandvehicles.presenters.VehiclesPresenter
import com.blackproxy.starwarsshipsandvehicles.presenters.VehiclesPresenterImpl
import com.blackproxy.starwarsshipsandvehicles.utils.MarginItemDecoration
import kotlinx.android.synthetic.main.fragment_vehicles.*

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
class VehiclesFragment: Fragment(), VehiclesView{


    private val presenter : VehiclesPresenter = VehiclesPresenterImpl(this)

    private var vehiclesAdapter = VehiclesAdapter(emptyList())

    private var vehicleList : ArrayList<Vehicle>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vehicles, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
        if (savedInstanceState !=null){
            vehicleList = savedInstanceState.getParcelableArrayList(VEHICLE_LIST_KEY)
            setItems(vehicleList)
        } else {
            if (vehicleList != null) {
                //donothing
            } else {
                presenter.loadVehicles()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(VEHICLE_LIST_KEY, vehicleList)
    }

    private fun initUI(){
        rvVehicles.adapter = vehiclesAdapter
        rvVehicles.layoutManager = LinearLayoutManager(activity)
        rvVehicles.addItemDecoration(MarginItemDecoration(resources.getDimension(R.dimen.default_padding).toInt()))
        swipeRefreshVehicles.setOnRefreshListener { presenter.loadVehicles() }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showProgress() {
        swipeRefreshVehicles.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefreshVehicles.isRefreshing = false
    }

    override fun setItems(items: List<Vehicle>?) {
        if (!items.isNullOrEmpty()){
            vehiclesAdapter.items = items
            refreshVehicleList()
        }
    }

    private fun refreshVehicleList(){
        vehiclesAdapter.notifyDataSetChanged()
    }

    override fun showMessage(message: String) {
        Toast.makeText(activity,message,Toast.LENGTH_LONG).show()
    }

    companion object {

        const val VEHICLE_LIST_KEY =  "VEHICLE_LIST_KEY"

        @JvmStatic
        fun newInstance() : VehiclesFragment {
            val fragment = VehiclesFragment()
            val arguments = Bundle()
            fragment.arguments = arguments
            return fragment
        }
    }
}