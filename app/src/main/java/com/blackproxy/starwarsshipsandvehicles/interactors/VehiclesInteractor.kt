package com.blackproxy.starwarsshipsandvehicles.interactors

import com.blackproxy.starwarsshipsandvehicles.model.Vehicle

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
interface VehiclesInteractor {

    fun getVehicles(successHandler: (List<Vehicle>?) -> Unit, failureHandler: (Throwable?) -> Unit)
}