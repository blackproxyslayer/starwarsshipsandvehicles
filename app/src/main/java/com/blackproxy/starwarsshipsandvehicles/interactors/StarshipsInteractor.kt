package com.blackproxy.starwarsshipsandvehicles.interactors

import com.blackproxy.starwarsshipsandvehicles.model.Starship

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
interface StarshipsInteractor {

    fun getStarships(successHandler: (List<Starship>?) -> Unit, failureHandler: (Throwable?) -> Unit)
}