package com.blackproxy.starwarsshipsandvehicles.interactors

import com.blackproxy.starwarsshipsandvehicles.api.Swapi
import com.blackproxy.starwarsshipsandvehicles.model.Starship
import com.blackproxy.starwarsshipsandvehicles.model.StarshipsList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
class StarshipsInteractorImpl: StarshipsInteractor {

    private val apiService = Swapi.getInstance()

    override fun getStarships(successHandler: (List<Starship>?) -> Unit, failureHandler: (Throwable?) -> Unit) {
        apiService!!.getStarships().enqueue(object : Callback<StarshipsList> {
            override fun onResponse(call: Call<StarshipsList>?, response: Response<StarshipsList>?) {
                response?.body()?.let {
                    successHandler(it.starshipsList)
                }
            }

            override fun onFailure(call: Call<StarshipsList>?, t: Throwable?) {
                failureHandler(t)
            }
        })
    }
}