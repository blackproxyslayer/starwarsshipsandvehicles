package com.blackproxy.starwarsshipsandvehicles.interactors

import com.blackproxy.starwarsshipsandvehicles.api.Swapi
import com.blackproxy.starwarsshipsandvehicles.model.StarshipsList
import com.blackproxy.starwarsshipsandvehicles.model.Vehicle
import com.blackproxy.starwarsshipsandvehicles.model.VehiclesList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Miguelete on 01/06/2019.
 * mShop Spain
 */
class VehiclesInteractorImpl : VehiclesInteractor {

    private val apiService = Swapi.getInstance()

    override fun getVehicles(successHandler: (List<Vehicle>?) -> Unit, failureHandler: (Throwable?) -> Unit) {
        apiService!!.getVehicles().enqueue(object : Callback<VehiclesList> {
            override fun onResponse(call: Call<VehiclesList>?, response: Response<VehiclesList>?) {
                response?.body()?.let {
                    successHandler(it.vehiclesList)
                }
            }

            override fun onFailure(call: Call<VehiclesList>?, t: Throwable?) {
                failureHandler(t)
            }
        })
    }
}
